import pickle
import argparse
import cv2
import numpy as np
import os

from tensorflow.keras.models import load_model


class Violenceradar:

    def __init__(self):
        base_path = os.path.dirname(os.path.abspath(__file__))
        self.model = load_model(base_path + '/data/violence_resnet.model')
        self.lb = pickle.loads(open(base_path + '/data/lb_resnet.pickle', "rb").read())
        self.mean = np.array([123.68, 116.779, 103.939][::1], dtype="float32")
        self.IMAGE_SIZE = 299

    def predict(self, image):
        image_pred = cv2.imread(image)
        image_pred = cv2.cvtColor(image_pred, cv2.COLOR_BGR2RGB)
        image_pred = cv2.resize(image_pred, (self.IMAGE_SIZE, self.IMAGE_SIZE)).astype("float32")
        image_pred -= self.mean

        prediction = self.model.predict(np.expand_dims(image_pred, axis=0))[0]
        label = self.lb.classes_[np.argmax(prediction)]
        value = prediction[np.argmax(prediction)]
        
        return label, value
