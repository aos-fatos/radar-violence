from setuptools import setup, Extension

setup(
    name='violenceradar',
    version='0.0.1',
    description="Violence radar can identify gore content, fireguns and fights in a input picture",
    author='Heitor Sampaio',
    author_email='horlando.heitor@gmail.com',
    license='MIT',
    packages=['violenceradar'],
    include_package_data=True,
    package_dir={'violenceradar': 'violenceradar'},
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3'
    ],
    install_requires=['tensorflow'],
    python_requires='>=3',
)
